<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

Lovely Menu admin backend developed with NestJs and GraphQL.

Used Authentication and guards.

Used Mongoose Paginate V2 to paginate queries results.

Used Validation Pipes.


## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev


## Stay in touch

- Authors - Julio Romero(julio_ale21@hotmail.com) - Eric Hidalgo

