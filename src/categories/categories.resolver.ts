import { PaginatedCategoryType } from './types/paginated-categories.type';
import { ValidationPipeInput } from './../validation/input.validation';
import { CurrentUser, GqlAuthGuard } from './../auth/Guards/gql-auth.guard';
import { CategoryType } from './types/category.type';
import { CategoriesService } from './categories.service';
import { Args, Mutation, Resolver, Query, Int } from "@nestjs/graphql";
import { UseGuards, UsePipes, UnauthorizedException, Logger } from '@nestjs/common';
import { User } from 'src/users/schemas/user.schema';
import { CategoryInput } from './inputs/category.input';
import * as mongoose from 'mongoose';


@Resolver()
export class CategoriesResolver {
    constructor(private readonly categoriesService: CategoriesService){}

    @Query(() => PaginatedCategoryType)
    @UseGuards(GqlAuthGuard)
    async getUserCategories(
        @CurrentUser() user: User,
        @Args('page', { type: () => Int, defaultValue: 1, nullable: true }) page: number,
        @Args('limit', { type: () => Int, defaultValue: 10, nullable: true }) limit: number) {
        return await this.categoriesService.findAllByCompany(user.company_id, page, limit);
    }

    @Query(() => CategoryType, { nullable: true })
    @UseGuards(GqlAuthGuard)
    async getCategoryById(
        @CurrentUser() user: User,
        @Args('id') id: string
    ) {
        const category = await this.categoriesService.findById(id);
        if (!mongoose.Types.ObjectId(category.company_id).equals(mongoose.Types.ObjectId(user.company_id))) throw new UnauthorizedException('User does not have permissions');
        return category;
    }

    @Mutation(returns => CategoryType)
    @UseGuards(GqlAuthGuard)
    @UsePipes(ValidationPipeInput)
    async createCategory(
        @CurrentUser() user : User,
        @Args('input') input: CategoryInput) {
        return await this.categoriesService.create(input, user.company_id);
    }

    @Mutation(() => CategoryType)
    @UseGuards(GqlAuthGuard)
    @UsePipes(ValidationPipeInput)
    async updateCategory(
        @CurrentUser() user: User,
        @Args('id') id: string, 
        @Args('input') input: CategoryInput
    ) {
        return await this.categoriesService.update(id, input, user.company_id);
    }

    @Mutation(() => String)
    @UseGuards(GqlAuthGuard)
    async deleteCategory(
        @CurrentUser() user: User,
        @Args('id') id: string
    ) {
        return await this.categoriesService.delete(id, user.company_id);
    }
}