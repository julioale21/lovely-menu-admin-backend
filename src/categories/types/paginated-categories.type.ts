import { CategoryType } from './category.type';
import { PaginatedType } from './../../paginated-type';
import { ObjectType, Field  } from "@nestjs/graphql";

@ObjectType()
export class PaginatedCategoryType extends PaginatedType {
    @Field(type => [CategoryType], { nullable: true })
    docs: CategoryType[];
}