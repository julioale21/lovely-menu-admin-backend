import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class CategoryType {
  @Field(() => ID)
  readonly _id?: string;

  @Field()
  readonly name: string;

  @Field(() => ID)
  readonly company_id: string;
}
