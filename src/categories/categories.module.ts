import { CategoryRepository } from './categories.repository';
import { Category, CategorySchema } from './schemas/category.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { CategoriesResolver } from './categories.resolver';
import { CategoriesService } from './categories.service';

@Module({
  imports: [
    MongooseModule.forFeature([{name:Category.name, schema: CategorySchema }])],
  providers: [CategoriesService, CategoriesResolver, CategoryRepository],
  exports: [CategoriesService]
})
export class CategoriesModule {}
