import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as aggregatePaginate  from 'mongoose-aggregate-paginate-v2';

@Schema()
export class Category extends Document {
    @Prop({ required: true })
    name: string;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref:"Company", required: true})
    company_id: string; 
}

export const CategorySchema = SchemaFactory.createForClass(Category).plugin(aggregatePaginate);
