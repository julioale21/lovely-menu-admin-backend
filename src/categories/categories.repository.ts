import { InjectModel } from "@nestjs/mongoose";
import { AgregatePaginateModel } from "mongoose-aggregate-paginate-v2";
import { Category } from "./schemas/category.schema";
import { CategoryInput } from "./inputs/category.input";
import { CategoryType } from "./types/category.type";
import { PaginatedCategoryType } from "./types/paginated-categories.type";
import * as mongoose from 'mongoose';


export class CategoryRepository {
    constructor(@InjectModel(Category.name) private readonly categoryModel: AgregatePaginateModel<Category>) {}

    async create(input: CategoryInput, companyId): Promise<CategoryType>{
        const createdCategory = new this.categoryModel({...input, company_id: companyId});
        return await createdCategory.save();
    }

    async findAllByCompany(companyId: string, page: number, limit: number): Promise<PaginatedCategoryType[]> {
        const options = {
            limit,
            page
        }

        var myAggregate = this.categoryModel.aggregate(
            [   { $match : { "company_id" : mongoose.Types.ObjectId(companyId)}},
                { $sort : { name : 1 } } 
            ]
        )
        
        return await this.categoryModel.aggregatePaginate(myAggregate, options);
    }

    async findById(id: string): Promise<CategoryType> {
        return await this.categoryModel.findById(id);
    }

    async updateById(id: string, input: CategoryInput): Promise<CategoryType> {
        return await this.categoryModel.findByIdAndUpdate(id, input, { new: true });
    }

    async deleteById(id: string) {
        return await this.categoryModel.findByIdAndDelete(id);
    }
}