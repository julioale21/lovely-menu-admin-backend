import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CategoryInput {
  @Field()
  @IsNotEmpty()
  readonly name: string;
}