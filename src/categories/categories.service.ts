import { CategoryRepository } from './categories.repository';
import { Category } from './schemas/category.schema';
import { Injectable, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CategoryType } from './types/category.type';
import { CategoryInput } from './inputs/category.input';
import { AgregatePaginateModel } from "mongoose-aggregate-paginate-v2";
import * as mongoose from 'mongoose';
import { PaginatedCategoryType } from './types/paginated-categories.type';

@Injectable()
export class CategoriesService {
    constructor(private categoryRepository: CategoryRepository){}

    async create(input: CategoryInput, companyId): Promise<CategoryType>{
        return await  this.categoryRepository.create(input, companyId);
    }

    async findAllByCompany(companyId: string, page: number, limit: number): Promise<PaginatedCategoryType[]> {
        return await this.categoryRepository.findAllByCompany(companyId, page, limit);
    }

    async findById(id: string): Promise<CategoryType> {
        return await this.categoryRepository.findById(id);
    }

    async update(id: string, input: CategoryInput, companyId: string): Promise<CategoryType> {
        const category = await this.categoryRepository.findById(id);
        if (!category) throw new NotFoundException('Category does not exists');
        if (category.company_id + '' != companyId) throw new UnauthorizedException('Current user does not have permissions');
        return await this.categoryRepository.updateById(id, input);
    }

    async delete(id: string, companyId: string): Promise<String> {
        const category = await this.categoryRepository.findById(id);
        if (!category) throw new NotFoundException('Category does not exists');
        if(! mongoose.Types.ObjectId(category.company_id).equals(mongoose.Types.ObjectId(companyId))) throw new UnauthorizedException('Current user does not have permissions');

        await this.categoryRepository.deleteById(id);
        return 'Category successfully deleted';
    }

}
