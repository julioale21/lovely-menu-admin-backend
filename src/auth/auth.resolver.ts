import { ValidationPipeInput } from './../validation/input.validation';
import { UserInputError } from 'apollo-server-express';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { Resolver, Mutation, Args } from "@nestjs/graphql";
import { AuthUserInput } from './auth-user.input';
import { UsePipes } from '@nestjs/common';

@Resolver()
export class AuthResolver {
    constructor(
        private authService: AuthService,
        private jwtService: JwtService
    ){}

    @Mutation(() => String)
    @UsePipes(ValidationPipeInput)
    async doLogin(@Args('input') authInput: AuthUserInput ): Promise<String> {
        const user = await this.authService.validateUser(authInput.email, authInput.password);
        
        const payload = { id: user.id, name: user.name, company_id: user.company_id };
        const token = this.jwtService.sign(payload);
        return token;
    }
}