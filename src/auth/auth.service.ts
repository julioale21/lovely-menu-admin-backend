import { Injectable, NotFoundException, Logger } from '@nestjs/common';
import { UsersService } from "../users/users.service";
import * as bcrypt from 'bcrypt';
import { AuthenticationError } from 'apollo-server-express';

@Injectable()
export class AuthService {
    constructor(private userService: UsersService) {}

    async validateUser(email: string, password: string): Promise<any> {
        const user = await this.userService.findOneByEmail(email);
        Logger.log('user: ', email)
        if (!user) throw new AuthenticationError('User not found');
        
        const correctPassword = await bcrypt.compare(password, user.password);
        if (!correctPassword) throw new AuthenticationError('Invalid password');

        return user;
    }
}
