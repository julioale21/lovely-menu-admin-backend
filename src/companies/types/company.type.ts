import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class CompanyType {
  @Field(() => ID)
  readonly id?: string;

  @Field()
  readonly name: string;
}