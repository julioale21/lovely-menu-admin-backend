import { InjectModel } from "@nestjs/mongoose";
import { Company } from "./schemas/company.schema";
import { Model } from 'mongoose';
import { CompanyType } from "./types/company.type";



export class  CompaniesRepository {
    constructor(@InjectModel(Company.name) private readonly companiesModule: Model<Company>) {}

    async create(name: string): Promise<CompanyType> {
        const companyCreated = new this.companiesModule({name});
        return await companyCreated.save();
    }
}