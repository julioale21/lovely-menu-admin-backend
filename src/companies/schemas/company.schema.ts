import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from 'mongoose'

@Schema()
export class Company extends Document {

    @Prop({ required: true, unique: true })
    name: string;
}

export const CompanySchema = SchemaFactory.createForClass(Company);