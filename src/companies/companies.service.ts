import { CompanyType } from './types/company.type';
import { Company } from './schemas/company.schema';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CompaniesRepository } from './companies.repository';

@Injectable()
export class CompaniesService {
    constructor(private readonly companiesRepository: CompaniesRepository) {}

    async create(name: string): Promise<CompanyType> {
        return await this.companiesRepository.create(name);
    }
}
