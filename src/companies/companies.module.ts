import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { CompaniesService } from './companies.service';
import { CompanySchema } from './schemas/company.schema';
import { CompaniesRepository } from './companies.repository';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Company', schema: CompanySchema }])],
  providers: [CompaniesService, CompaniesRepository],
  exports: [CompaniesService]
})
export class CompaniesModule {}
