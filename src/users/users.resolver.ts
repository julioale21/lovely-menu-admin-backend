import { PaginatedUsersType } from './types/paginated-users.type';
import { ValidationPipeInput } from './../validation/input.validation';
import { GqlAuthGuard } from './../auth/Guards/gql-auth.guard';
import { UsersService } from './users.service';
import { UserInput } from './inputs/user.input';
import { UserType } from './types/user.type';
import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { UseGuards, UsePipes } from '@nestjs/common';

@Resolver()
export class UsersResolver {
    constructor(private readonly userService: UsersService) {}

    @Query(() => PaginatedUsersType)
    @UseGuards(GqlAuthGuard)
    async getUsers(
        @Args('page', { type: () => Int, defaultValue: 1, nullable: true }) page: number,
        @Args('limit', { type: () => Int, defaultValue: 10, nullable: true }) limit: number,
    ) {
        return await this.userService.findAll(page, limit);
    }

    @Query(() => UserType,  { nullable: true })
    async findUserByEmail(@Args('email') email: string) {
        return await this.userService.findOneByEmail(email);
    }

    @Mutation(() => UserType)
    @UsePipes(ValidationPipeInput)
    async createUser(@Args('input') input: UserInput) {
        return await this.userService.create(input);
    }

    @Mutation(() => UserType)
    @UsePipes(ValidationPipeInput)
    async updateUser(
        @Args('id') id: string, 
        @Args('input') input: UserInput
    ) {
        return await this.userService.update(id, input);
    }

    @Mutation(() => String)
    async deleteUser(@Args('id') id: string) {
        return await this.userService.delete(id);
    }

}
