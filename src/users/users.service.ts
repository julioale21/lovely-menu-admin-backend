import { PaginatedUsersType } from './types/paginated-users.type';
import { UserType } from './types/user.type';
import { UserInput } from './inputs/user.input';
import { User } from '../users/schemas/user.schema';
import { Injectable, ConflictException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { AgregatePaginateModel } from "mongoose-aggregate-paginate-v2";
import * as bcrypt from 'bcrypt';
import { CompaniesService } from 'src/companies/companies.service';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel('User') private userModel: AgregatePaginateModel<User>,
        private readonly companiesService:CompaniesService
        ) {}

    async create(createUserDto: UserInput): Promise<UserType> {
        const userExists = await this.findOneByEmail(createUserDto.email);
        if (userExists) throw new ConflictException('User already exist');

        const user = {...createUserDto}
        const salt = bcrypt.genSaltSync(10);

        const company = await this.companiesService.create(user.companyName);
        user['company_id'] = company.id;
        user.password = bcrypt.hashSync(createUserDto.password, salt);

        const createdUser = new this.userModel(user);
        return await createdUser.save();
    }

    async findOneByEmail(email: string): Promise<UserType> {
        return await this.userModel.findOne({email});
    }

    async findById(id: string): Promise<UserType> {
        return this.userModel.findById(id);
    }

    async findAll(page: number, limit: number): Promise<PaginatedUsersType> {

        const options = {
            page,
            limit
        }

        const aggregate = this.userModel.aggregate();

        return this.userModel.aggregatePaginate(aggregate, options);
    }

    async update(id: string, input: UserInput): Promise<UserType> {
        const exists = await this.findById(id);
        if (!exists) throw new NotFoundException('User not found')

        const user = {...input}
        const salt = bcrypt.genSaltSync(10);
        user.password = bcrypt.hashSync(input.password, salt);

        return await this.userModel.findByIdAndUpdate(id, input, { new: true });
    }

    async delete(id: string): Promise<String> {
        const user = await  this.userModel.findById(id);
        if(!user) throw new NotFoundException('User does not exists');
        await this.userModel.findByIdAndDelete(id);
        return 'User successfully deleted'
    }

}
