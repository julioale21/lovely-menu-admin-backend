import { UserType } from './user.type';
import { PaginatedType } from './../../paginated-type';
import { ObjectType, Field  } from "@nestjs/graphql";

@ObjectType()
export class PaginatedUsersType extends PaginatedType {
    @Field(type => [UserType], { nullable: true })
    docs: UserType[];
}