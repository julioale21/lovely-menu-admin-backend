import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoose from 'mongoose';
import * as aggregatePaginate  from 'mongoose-aggregate-paginate-v2';

@Schema()
export class User extends Document {
    @Prop({ required: true })
    name: string;

    @Prop({ required: true, unique: true })
    email: string;

    @Prop({ required: true })
    password: string;

    @Prop({ default: '' })
    imageURL: string

    @Prop({type: mongoose.Schema.Types.ObjectId, ref:"Company", required: true})
    company_id: string;
}

export const UserSchema = SchemaFactory.createForClass(User).plugin(aggregatePaginate);
