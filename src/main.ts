import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  const port = process.env.PORT || 3333
  await app.listen(3000, () => {
    Logger.log('Listening at http://localhost:' + port + '/')
  });
}
bootstrap();
