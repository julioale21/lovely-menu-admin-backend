import { ProductsRepository } from './products.repository';
import { Product, ProductSchema } from './schemas/product.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsResolver } from './products.resolver';
import { CategoriesModule } from 'src/categories/categories.module';


@Module({
  imports: [
    CategoriesModule,
    MongooseModule.forFeature([{name: Product.name, schema: ProductSchema}])],
  providers: [ProductsService, ProductsResolver, ProductsRepository],
  exports: [ProductsService]
})
export class ProductsModule {}
