import { IsNotEmpty } from 'class-validator';
import { InputType, Field } from "@nestjs/graphql";

@InputType()
export class ProductInput {
    @Field()
    @IsNotEmpty()
    name: string

    @Field()
    @IsNotEmpty()
    price: number

    @Field({defaultValue: '', nullable: true})
    description: string

    @Field({defaultValue: 0, nullable: true})
    rank: number

    @Field({defaultValue: 'active', nullable: true})
    status: string

    @Field({defaultValue: '', nullable: true})
    imageURL: string

    @Field()
    @IsNotEmpty()
    category_id: string
}