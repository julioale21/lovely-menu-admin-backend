import { PaginatedType } from './../../paginated-type';
import { ProductType } from './product.type';
import { ObjectType, Field  } from "@nestjs/graphql";

@ObjectType()
export class PaginatedProductType extends PaginatedType {
    @Field(type => [ProductType], { nullable: true })
    docs: ProductType[];
}