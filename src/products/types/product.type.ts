import { ObjectType, Field, ID } from "@nestjs/graphql";

@ObjectType()
export class ProductType {
    @Field(() => ID)
    readonly _id?: string;

    @Field({nullable: true})
    readonly name: string;

    @Field({nullable: true})
    readonly description: string;

    @Field({nullable: true})
    readonly rank: number;

    @Field({nullable: true})
    readonly status: string;

    @Field({nullable: true})
    readonly price: number;

    @Field({nullable: true})
    readonly imageURL: string;

    @Field(() => ID)
    readonly category_id: string;

    @Field({nullable: true})
    readonly category_name: string;
}