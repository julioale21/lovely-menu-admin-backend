import { ProductsRepository } from './products.repository';
import { PaginatedProductType } from './types/paginated-products.type';
import { ProductType } from './types/product.type';
import { ProductInput } from './inputs/product.input';
import { Product } from './schemas/product.schema';
import { Injectable, UnauthorizedException, NotFoundException } from '@nestjs/common';
import { CategoriesService } from 'src/categories/categories.service';

@Injectable()
export class ProductsService {
    constructor(
        private productsRepository: ProductsRepository,
        private readonly categoryService: CategoriesService) {}

    async getAllByCategory(categoryId: string, companyId: string, page: number, limit: number): Promise<PaginatedProductType> {
        const category = await this.categoryService.findById(categoryId);
        if (category.company_id + '' != companyId) throw new UnauthorizedException('User does not have permissions');

        return this.productsRepository.getAllByCategory(categoryId, page, limit);
    }

    async getAll(companyId: string, page: number, limit: number): Promise<PaginatedProductType> {
        return this.productsRepository.getAll(companyId, page, limit);
    }

    async getById(id: string): Promise<Product> {
        return await this.productsRepository.getById(id);
    }

    async create(productInput: ProductInput): Promise<ProductType>{
        return this.productsRepository.create(productInput);
    }

    async update(id: string, input: ProductInput, companyId: string): Promise<ProductType> {
        const category = await this.categoryService.findById(input.category_id);
        if ( category.company_id + '' != companyId) throw new UnauthorizedException('User does not have permissions');

        return await this.productsRepository.update(id, input);
    }

    async remove(id: string, companyId: string): Promise<String> {
        const product = await this.productsRepository.getById(id);
        if (!product) throw new NotFoundException('Product not found');

        const category = await this.categoryService.findById(product.category_id);
        if ( category.company_id + '' != companyId) throw new UnauthorizedException('User does not have permissions');

        return await this.productsRepository.remove(id);
    }

}
