import { Product } from './schemas/product.schema';
import { InjectModel } from "@nestjs/mongoose";
import { AgregatePaginateModel } from "mongoose-aggregate-paginate-v2";
import { PaginatedProductType } from './types/paginated-products.type';
import * as mongoose from 'mongoose';
import { ProductInput } from './inputs/product.input';
import { ProductType } from './types/product.type';

export class ProductsRepository {
    constructor(@InjectModel(Product.name) private readonly productModel: AgregatePaginateModel<Product>){}

    async getAllByCategory(categoryId: string, page: number, limit: number): Promise<PaginatedProductType> {
        
        const options = {
            limit,
            page
        }

        var myAggregate = this.productModel.aggregate(
            [   { $match : { "category_id" : mongoose.Types.ObjectId(categoryId)}},
                { $sort : { name : 1 } } 
            ]
        )

        const result = await this.productModel.aggregatePaginate(myAggregate, options);
        return result;
    }

    async getAll(companyId: string, page: number, limit: number): Promise<PaginatedProductType> {

        const options = {
            limit,
            page
        }

        const aggregate = this.productModel.aggregate([
            {
                $lookup: {
                    from: 'categories',
                    localField: 'category_id',
                    foreignField: '_id',
                    as: 'category'
                }
            },
            {   $unwind:"$category" },
            {
                $lookup: {
                    from: 'companies',
                    localField: 'category.company_id',
                    foreignField: '_id',
                    as: 'company'
                }
            },
            {   $unwind:"$company" },
            {
                $match:{
                    $and:[{"company._id" : mongoose.Types.ObjectId(companyId)}]
                }
            },
            { $sort : { name : 1 } },
            { $project: {
                _id: 1,
                rank: 1,
                status: 1,
                name: 1,
                price: 1,
                description: 1,
                imageURL: 1,
                category_id: 1,
                category_name: "$category.name"
            }}
        ])

        const result = await this.productModel.aggregatePaginate(aggregate, options)
        return result;
    }

    async getById(id: string): Promise<Product> {
        return await this.productModel.findById(id);
    }

    async create(productInput: ProductInput): Promise<ProductType>{
        const createdProduct = new this.productModel(productInput);
        return await createdProduct.save();
    }

    async update(id: string, input: ProductInput): Promise<ProductType> {
        const updatedProduct = await this.productModel.findByIdAndUpdate(id, input, { new: true })
        return updatedProduct;
    }

    async remove(id: string): Promise<String> {
        await this.productModel.findByIdAndDelete(id);
        return 'Product successfully deleted';
    }
}