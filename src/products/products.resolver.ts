import { User } from 'src/users/schemas/user.schema';
import { ValidationPipeInput } from './../validation/input.validation';
import { CurrentUser, GqlAuthGuard } from './../auth/Guards/gql-auth.guard';
import { ProductInput } from './inputs/product.input';
import { ProductType } from './types/product.type';
import { ProductsService } from './products.service';
import { Resolver, Mutation, Args, Query, Int } from "@nestjs/graphql";
import { UseGuards, UsePipes } from '@nestjs/common';
import { PaginatedProductType } from './types/paginated-products.type';

@Resolver()
export class ProductsResolver {
    constructor(private readonly productsService: ProductsService) {}

    @Query(() => PaginatedProductType)
    @UseGuards(GqlAuthGuard)
    async getProductsByCategory(
            @CurrentUser() user: User,
            @Args('categoryId') categoryId: string,
            @Args('page', { type: () => Int, defaultValue: 1, nullable: true }) page: number,
            @Args('limit', { type: () => Int, defaultValue: 10, nullable: true }) limit: number) {
        return await this.productsService.getAllByCategory(categoryId, user.company_id, page, limit);
    }

    @Query(() => PaginatedProductType)
    @UseGuards(GqlAuthGuard)
    async getProducts(
        @CurrentUser() user: User,
        @Args('page', { type: () => Int, defaultValue: 1, nullable: true }) page: number,
        @Args('limit', { type: () => Int, defaultValue: 10, nullable: true }) limit: number) {
            return await this.productsService.getAll(user.company_id, page, limit);
    }

    @Query(() => ProductType)
    @UseGuards(GqlAuthGuard)
    async getProductById(@Args('id') id: string) {
        return await this.productsService.getById(id);
    }

    @Mutation(() => ProductType)
    @UseGuards(GqlAuthGuard)
    @UsePipes(ValidationPipeInput)
    async createProduct(@Args('input') input: ProductInput) {
        return await this.productsService.create(input);
    }

    @Mutation(() => ProductType)
    @UseGuards(GqlAuthGuard)
    @UsePipes(ValidationPipeInput)
    async updateProduct(
        @CurrentUser() user: User,
        @Args('id') id: string,
        @Args('input') input: ProductInput
    ) {
        return await this.productsService.update(id, input, user.company_id); 
    }

    @Mutation(() => String)
    @UseGuards(GqlAuthGuard)
    async deleteProduct(
        @CurrentUser() user: User,
        @Args('id') id: string
    ) {
        return await this.productsService.remove(id, user.company_id);
    }
}