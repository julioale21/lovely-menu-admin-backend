import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes } from "mongoose";
import * as aggregatePaginate  from 'mongoose-aggregate-paginate-v2';

@Schema()
export class Product extends Document {
    @Prop({ required: true })
    name: string

    @Prop({ required: true})
    price: number

    @Prop({ required:false, default: 'active'})
    status: string

    @Prop({ required:false, default: 0 })
    rank: number

    @Prop({ required: false})
    description: string

    @Prop({ required: false})
    imageURL: string

    @Prop({ required: true, type: SchemaTypes.ObjectId })
    category_id: string
}

export const ProductSchema = SchemaFactory.createForClass(Product).plugin(aggregatePaginate);
