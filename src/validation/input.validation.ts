import { ArgumentMetadata, BadRequestException, ValidationPipe, Logger } from '@nestjs/common';
import { UserInputError } from 'apollo-server-express';
import { getDescription } from 'graphql';

export class ValidationPipeInput extends ValidationPipe {
    public async transform(value, metadata: ArgumentMetadata){
        try {
            return await super.transform(value, metadata)
        } catch (e) {
            let finalErr = e;
            if (e instanceof BadRequestException && metadata.type === 'body') {
                throw  new UserInputError('User input error', {
                    invalidArgs: Object.assign(
                        {},
                        ...e.getResponse()['message'].map((message) => ({
                            ['description']: message
                        }))
                    )
                });
            }
        }
    }
}