import { ObjectType, Field  } from "@nestjs/graphql";

@ObjectType()
export class PaginatedType {
    @Field()
    readonly totalDocs: number;

    @Field({nullable: true})
    readonly offset: number;

    @Field({nullable: true})
    readonly limit: number;

    @Field({nullable: true})
    readonly totalPages: number;

    @Field({nullable: true})
    readonly page: number;

    @Field({nullable: true})
    readonly pagingCounter: number;

    @Field({nullable: true})
    readonly hasPrevPage: boolean;

    @Field({nullable: true})
    readonly hasNextPage: boolean;

    @Field({nullable: true})
    readonly prevPage: number;

    @Field({nullable: true})
    readonly nextPage: number;
}