import { ProductsModule } from './products/products.module';
import { validationSchema } from './config/validation';
import { configuration } from './config/configuration';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { GraphQLModule } from "@nestjs/graphql";
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { CategoriesModule } from './categories/categories.module';
import { CompaniesModule } from './companies/companies.module';


@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: true,
      load: [configuration],
      validationSchema
    }),
    PassportModule.register({ defaultStrategy: 'jwr'}),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      installSubscriptionHandlers: true,
      context: ({req, res}) => ({req, res})
    }),
    MongooseModule.forRoot(
      process.env.MONGO_URI
    ),
    UsersModule,
    AuthModule,
    CategoriesModule,
    CompaniesModule,
    ProductsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
